<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "title">
        ${msg("registerWithTitle",(realm.displayName!''))}
    <#elseif section = "header">
        ${msg("registerWithTitleHtml",(realm.displayNameHtml!''))?no_esc}
    <#elseif section = "form">
        <div class="row">
            <div class="col-md-12 cognitiva-subtitle">Crea una nueva cuenta</div>
        </div>
        <form id="kc-register-form" class="${properties.kcFormClass!}" action="${url.registrationAction}" method="post">
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('firstName',properties.kcFormGroupErrorClass!)}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="firstName" class="${properties.kcLabelClass!}" style="font-weight: 400; font-family: DINNextLT;">${msg("firstName")}</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px; font-family: DINNextLT;">
                    <input type="text" id="firstName" class="${properties.kcInputClass!}" name="firstName" value="${(register.formData.firstName!'')}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('lastName',properties.kcFormGroupErrorClass!)}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="lastName" class="${properties.kcLabelClass!}" style="font-weight: 400; font-family: DINNextLT;">${msg("lastName")}</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px; font-family: DINNextLT;">
                    <input type="text" id="lastName" class="${properties.kcInputClass!}" name="lastName" value="${(register.formData.lastName!'')}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('email',properties.kcFormGroupErrorClass!)}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="email" class="${properties.kcLabelClass!}" style="font-weight: 400; font-family: DINNextLT;">${msg("email")}</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px; font-family: DINNextLT;">
                    <input type="text" id="email" class="${properties.kcInputClass!}" name="email" value="${(register.formData.email!'')}" autocomplete="email" />
                </div>
            </div>

          <#if !realm.registrationEmailAsUsername>
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('username',properties.kcFormGroupErrorClass!)}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="username" class="${properties.kcLabelClass!}" style="font-weight: 400; font-family: DINNextLT;">${msg("username")}</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px; font-family: DINNextLT;">
                    <input type="text" id="username" class="${properties.kcInputClass!}" name="username" value="${(register.formData.username!'')}" autocomplete="username" />
                </div>
            </div>
          </#if>

            <#if passwordRequired>
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('password',properties.kcFormGroupErrorClass!)}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="password" class="${properties.kcLabelClass!}" style="font-weight: 400; font-family: DINNextLT;">${msg("password")}</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px; font-family: DINNextLT;">
                    <input type="password" id="password" class="${properties.kcInputClass!}" name="password" autocomplete="new-password" autocomplete="off"/>
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('password-confirm',properties.kcFormGroupErrorClass!)}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="password-confirm" class="${properties.kcLabelClass!}" style="font-weight: 400; font-family: DINNextLT;">${msg("passwordConfirm")}</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px; font-family: DINNextLT;">
                    <input type="password" id="password-confirm" class="${properties.kcInputClass!}" name="password-confirm" autocomplete="off"/>
                </div>
            </div>
            </#if>

            <div class="form-group">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="user.attributes.company" class="${properties.kcLabelClass!}" style="font-weight: 400; font-family: DINNextLT;">Nombre del Negocio</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px; font-family: DINNextLT;">
                    <input type="text" id="user.attributes.company" class="${properties.kcInputClass!}" name="user.attributes.company" />
                </div>
            </div>

            <div class="form-group" style="display: none;">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.parent" class="${properties.kcLabelClass!}">Parent</label>
                </div>

                <div class="col-sm-10 col-md-10">
                    <input type="text" checked class="${properties.kcInputClass!}"  id="user.attributes.parent" name="user.attributes.parent" value="true"/>
                </div>
            </div>

            <#if recaptchaRequired??>
            <div class="form-group">
                <div class="${properties.kcInputWrapperClass!}">
                    <div class="g-recaptcha" data-size="compact" data-sitekey="${recaptchaSiteKey}"></div>
                </div>
            </div>
            </#if>

            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="font-family: DINNextLT;">
                    <span><a href="${url.loginUrl}">${msg("backToLogin")?no_esc}</a></span>
                </div>

                <div id="kc-form-buttons" class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding-right: 0;">
                    <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" type="submit" value="${msg("doRegister")}" style="float: right; font-weight: 400 !important; font-family: DINNextLT;"/>
                </div>
            </div>
        </form>
    </#if>
</@layout.registrationLayout>